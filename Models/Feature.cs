using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace web_api.Models
{
    public class Feature
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public ICollection<VehicleFeatures> Vehicles { get; set; }
        public Feature()
        {
            Vehicles = new Collection<VehicleFeatures>();
        }
    }
}