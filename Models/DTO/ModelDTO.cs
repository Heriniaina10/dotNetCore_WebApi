using System.ComponentModel.DataAnnotations;

namespace web_api.Models.DTO
{
    public class ModelDTO
    {
        public int Id { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
    }
}