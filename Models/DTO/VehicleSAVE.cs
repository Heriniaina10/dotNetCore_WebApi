using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace web_api.Models.DTO
{
    public class VehicleSAVE
    {
        public int Id { get; set; }
        public bool IsRegistered { get; set; }
        public ContactDTO Contact { get; set; }
        public int ModelId { get; set; }
        public ICollection<int> Features { get; set; }

        public VehicleSAVE()
        {
            Features = new Collection<int>();
        }
    }
}