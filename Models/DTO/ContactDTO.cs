using System.ComponentModel.DataAnnotations;

namespace web_api.Models.DTO
{
    public class ContactDTO
    {
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        [StringLength(255)]
        public string Email { get; set; }
        [Required]
        [StringLength(255)]
        public string Phone { get; set; }
    }
}