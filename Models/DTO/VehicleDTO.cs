using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace web_api.Models.DTO
{
    public class VehicleDTO
    {
        public int Id { get; set; }
        public bool IsRegistered { get; set; }
        public ContactDTO Contact { get; set; }
        public int ModelId { get; set; }
        public ModelDTO Model { get; set; }
        public CommonDTO Make { get; set; }
        public ICollection<CommonDTO> Features { get; set; }

        public VehicleDTO()
        {
            Features = new Collection<CommonDTO>();
        }
    }
}