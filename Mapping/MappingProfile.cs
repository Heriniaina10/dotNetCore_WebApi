using System.Linq;
using AutoMapper;
using web_api.Models;
using web_api.Models.DTO;

namespace web_api.Mapping
{
     public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Vehicle, VehicleDTO>()
                .ForMember(dto => dto.Contact, opt => opt.MapFrom(v => new ContactDTO {
                    Name = v.ContactName,
                    Email = v.ContactEmail,
                    Phone = v.ContactPhone
                }))
                .ForMember(dto => dto.Features, opt => opt.MapFrom(v => v.Features.Select(f => new CommonDTO {
                    Id = f.Feature.Id,
                    Name = f.Feature.Name
                })))
                .ForMember(dto => dto.Make, opt => opt.MapFrom(v => v.Model.Make));

            CreateMap<VehicleSAVE, Vehicle>()
                .ForMember(v => v.Id, opt => opt.Ignore())
                .ForMember(v => v.ContactName, opt => opt.MapFrom(vs => vs.Contact.Name))
                .ForMember(v => v.ContactEmail, opt => opt.MapFrom(vs => vs.Contact.Email))
                .ForMember(v => v.ContactPhone, opt => opt.MapFrom(vs => vs.Contact.Phone))
                .ForMember(v => v.Features, opt => opt.Ignore())
                .AfterMap((dto, v) => {
                    // var toRemove = v.Features.Select(x => x.FeatureId).Except(dto.Features);
                    var toRemove = v.Features.Where(x => !dto.Features.Contains(x.FeatureId));
                    foreach (var f in toRemove)
                    {
                        v.Features.Remove(f);
                    }

                    // var toAdd = dto.Features.Except(v.Features.Select(x => x.FeatureId));
                    var toAdd = dto.Features.Where(id => !v.Features.Any(x => x.FeatureId == id))
                        .Select(id => new VehicleFeatures {
                            VehicleId = v.Id,
                            FeatureId = id
                        });
                    foreach (var f in toAdd)
                    {
                        v.Features.Add(f);
                    }
                });
        }
    }
}