using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using web_api.Models;
using web_api.Models.DTO;
using web_api.Persistence;
using web_api.Repository;

namespace web_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly AppDbContext _ctx;
        private IVehicleRepository _repository;
        private readonly IModelRepository _modelRepository;
        private IUnitOfWork<AppDbContext> _unitOfWork;
        private readonly IMapper _mapper;

        public VehiclesController(AppDbContext ctx, IVehicleRepository repository, IModelRepository modelRepository,IUnitOfWork<AppDbContext> unitOfWork, IMapper mapper)
        {
            _ctx = ctx;
            _repository = repository;
            _modelRepository = modelRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        // GET api/vehicles
        [HttpGet("{vehicleId}")]
        public async Task<IActionResult> GetVehicle(int vehicleId)
        {
            var vehicle = await _repository.GetVehicle(vehicleId);
            if (vehicle == null) {
                return NotFound();
            }

            return Ok(_mapper.Map<Vehicle, VehicleDTO>(vehicle));
        }

        // POST api/vehicles
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] VehicleSAVE vehicleDTO)
        {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var model = await _modelRepository.GetModel(vehicleDTO.ModelId);
            if (model == null) {
                ModelState.AddModelError("Model", "Invalid ModelId.");
                return BadRequest(ModelState);
            }

            var vehicle = _mapper.Map<VehicleSAVE, Vehicle>(vehicleDTO);
            _repository.Add(vehicle);
            await _unitOfWork.Commit();

            return Ok(vehicle);
        }

        // PUT api/vehicles/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] VehicleSAVE vehicleSAVE)
        {
            var vehicle = await _repository.GetVehicle(id, includeRelated: false);
            if (vehicle == null) {
                return NotFound();
            }

            _mapper.Map<VehicleSAVE, Vehicle>(vehicleSAVE, vehicle);
            vehicle.LastUpdate = DateTime.Now;
            await _unitOfWork.Commit();

            return Ok(vehicle);
            // return Ok($"Modification effectuée (ID : {id})");
        }

        // DELETE api/vehicles/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var vehicle = await _ctx.Vehicles.FindAsync(id);
            if (vehicle == null) {
                return NotFound();
            }

            _ctx.Vehicles.Remove(vehicle);
            await _unitOfWork.Commit();

            return Ok($"Suppression effectuée (ID : {id})");
        }
    }
}
