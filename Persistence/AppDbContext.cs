using Microsoft.EntityFrameworkCore;
using web_api.Models;

namespace web_api.Persistence
{
    public class AppDbContext: DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // 01 Model ==> n Vehicles
            // modelBuilder.Entity<Vehicle>()
            //     .HasOne(x => x.Model)
            //     .WithMany(y => y.Vehicles)
            //     .HasForeignKey(x => x.ModelId);

            // 01 Make ==> n Model
            modelBuilder.Entity<Model>()
                .HasOne(x => x.Make)
                .WithMany(y => y.Models)
                .HasForeignKey(x => x.MakeId);

            // n Features ==> n Vehicles
            modelBuilder.Entity<VehicleFeatures>()
                .HasKey(vf => new {
                    vf.VehicleId,
                    vf.FeatureId
                });
        }

        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<Make> Makes { get; set; }
        public DbSet<Feature> Features { get; set; }
    }
}