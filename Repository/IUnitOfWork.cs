using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace web_api.Repository
{
   public interface IUnitOfWork<TContext> : IDisposable
    {
        // TContext GetContext();
        Task Commit();
        // void Dispose();
    }
}