using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using web_api.Models;
using web_api.Persistence;

namespace web_api.Repository
{
    public class VehicleRepository : IVehicleRepository
    {
        private readonly AppDbContext _ctx;

        public VehicleRepository(AppDbContext ctx)
        {
            _ctx = ctx;
        }

        public void Add(Vehicle vehicle)
        {
            vehicle.LastUpdate = DateTime.Now;
            _ctx.Vehicles.Add(vehicle);
        }

        public async Task<Vehicle> GetVehicle(int vehicleId, bool includeRelated = true)
        {
            if (!includeRelated) {
                return await _ctx.Vehicles.FirstOrDefaultAsync(v => v.Id == vehicleId);
            }

            return ( await _ctx.Vehicles
                .Include(x => x.Model)
                    .ThenInclude(y => y.Make)
                .Include(x => x.Features)
                    .ThenInclude(y => y.Feature)
                .FirstOrDefaultAsync(v => v.Id == vehicleId)
            );
        }

        public void Remove(Vehicle vehicle)
        {
            throw new System.NotImplementedException();
        }
    }
}