using System.Threading.Tasks;
using web_api.Models;
using web_api.Models.DTO;

namespace web_api.Repository
{
    public interface IVehicleRepository
    {
        Task<Vehicle> GetVehicle(int id, bool includeRelated = true);
        // Task<QueryResult<Vehicle>> GetVehicle(VehicleQuery filter);
        void Add(Vehicle vehicle);
        void Remove(Vehicle vehicle);
        // void Update(VehicleDTO vehicleDTO);
    }
}