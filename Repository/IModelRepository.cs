using System.Threading.Tasks;
using web_api.Models;

namespace web_api.Repository
{
    public interface IModelRepository
    {
        Task<Model> GetModel(int id, bool includeRelated = true);
        void Add(Model model);
        void Remove(Model model);
    }
}