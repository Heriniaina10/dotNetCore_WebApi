using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using web_api.Persistence;

namespace web_api.Repository
{
    public class UnitOfWork :  IUnitOfWork<AppDbContext>
    {
        public AppDbContext Context { get; }

        public UnitOfWork(AppDbContext context)
        {
            Context = context;
        }
        public async Task Commit()
        {
            await Context.SaveChangesAsync();
        }

        public void Dispose()
        {
           Context.Dispose();

        }

        // public AppDbContext GetContext()
        // {
        //     return Context;
        // }
    }
}