using System.Threading.Tasks;
using web_api.Models;
using web_api.Persistence;

namespace web_api.Repository
{
    public class ModelRepository : IModelRepository
    {
        private readonly AppDbContext _ctx;

        public ModelRepository(AppDbContext ctx)
        {
            _ctx = ctx;
        }

        public void Add(Model model)
        {
            throw new System.NotImplementedException();
        }

        public async Task<Model> GetModel(int id, bool includeRelated = true)
        {
            return await _ctx.Models.FindAsync(id);
        }

        public void Remove(Model model)
        {
            throw new System.NotImplementedException();
        }
    }
}